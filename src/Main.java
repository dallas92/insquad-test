import java.time.Duration;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/*
2 производителя, генерирующих арифметическую прогрессию (пример, {3,6,9,...} и {5,10,15,...}).
Производители работают параллельно и должны работать независимо друг от друга.
Каждый производитель должен генерировать числа через случайные промежутки времени.

Один принтер, который печатает выходные данные, сгенерированные всеми производителями.
Принтер должен печатать выходные данные в порядке LIFO.
Принтер может печатать только во время случайных окон печати.
Принтер не должен печатать ничего, когда он находится за пределами окна печати, и ждать следующего окна печати для дальнейшей печати.
Принтер должен работать параллельно с производителями.
 */
public class Main {

    private static final int MIN_VALUE = 1;
    private static final int MAX_VALUE = 10;
    private static final long WAIT_TIME_IN_MS = Duration.ofSeconds(6).toMillis();
    private static final Random randomizer = new Random();
    private static final List<Integer> values  = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        System.out.println("[Main]: started");

        var executorService = Executors.newFixedThreadPool(3);
        executorService.execute(new Manufacturer("Manufacturer 1", randomizer.nextInt(MAX_VALUE - MIN_VALUE) + MIN_VALUE));
        executorService.execute(new Manufacturer("Manufacturer 2", randomizer.nextInt(MAX_VALUE - MIN_VALUE) + MIN_VALUE));
        executorService.execute(new Printer("Printer"));

        Thread.sleep(WAIT_TIME_IN_MS);
        executorService.shutdownNow();

        System.out.println(String.format("[Main]: %s", values));
        System.out.println("[Main]: finished");
    }

    static abstract class Worker {

        private final int MIN_TIME_TO_SLEEP_IN_MS = 150;
        private final int MAX_TIME_TO_SLEEP_IN_MS = 650;
        private final Random randomizer = new Random();
        protected long extraTimeToSleepInMs = 0;
        protected final String name;

        public Worker(String name) {
            this.name = name;
        }

        public void doWork() {
            while (true) {
                try {
                    Thread.sleep(randomizer.nextInt(MAX_TIME_TO_SLEEP_IN_MS - MIN_TIME_TO_SLEEP_IN_MS) + MIN_TIME_TO_SLEEP_IN_MS + extraTimeToSleepInMs);
                    doJobDuringWork();
                } catch (InterruptedException e) {
                    finishWork();
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        }

        protected abstract void doJobDuringWork();

        public void finishWork() {

        }
    }

    static class Manufacturer extends Worker implements Runnable {

        private int firstValue;
        private int secondValue;

        public Manufacturer(String name, int value) {
            super(name);

            this.firstValue = 0;
            this.secondValue = value;
        }

        @Override
        public void run() {
            doWork();
        }

        @Override
        public void doJobDuringWork() {
            var nextValue = this.firstValue + this.secondValue;
            this.firstValue = this.secondValue;
            this.secondValue = nextValue;

            System.out.println(String.format("[%s]: generated %s", this.name, nextValue));
            synchronized (values) {
                values.add(nextValue);
            }
        }
    }

    static class Printer extends Worker implements Runnable {

        private long processedValuesCount;

        public Printer(String name) {
            super(name);

            this.processedValuesCount = 0;
            this.extraTimeToSleepInMs = Duration.ofSeconds(2).toMillis();
        }

        @Override
        public void run() {
            doWork();
        }

        @Override
        public void doJobDuringWork() {
            var valuesToPrint = values.stream().skip(processedValuesCount).collect(Collectors.toList());
            if (!valuesToPrint.isEmpty()) {
                Collections.reverse(valuesToPrint);
                System.out.println(String.format("[%s]: %s", this.name, valuesToPrint));
                processedValuesCount += valuesToPrint.size();
            }
        }

        @Override
        public void finishWork() {
            doJobDuringWork();
        }
    }
}
